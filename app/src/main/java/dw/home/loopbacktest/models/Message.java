package dw.home.loopbacktest.models;
import com.strongloop.android.loopback.Model;
/**
 * Created by MacOS on 8/02/16.
 *
 */
public class Message extends Model {
    private String name, description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
