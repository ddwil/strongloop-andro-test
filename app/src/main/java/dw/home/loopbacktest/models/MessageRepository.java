package dw.home.loopbacktest.models;

import com.strongloop.android.loopback.ModelRepository;

/**
 * Created by MacOS on 8/02/16.
 */
public class MessageRepository extends ModelRepository<Message> {
    public MessageRepository() {
        super("message", Message.class);
    }
}
