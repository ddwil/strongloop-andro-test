package dw.home.loopbacktest;

    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;

    import com.strongloop.android.loopback.RestAdapter;
    import com.strongloop.android.remoting.adapters.RestContractItem;

public class MainActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        }
        RestAdapter adapter;

        public RestAdapter getLoopBackAdapter() {
            if (adapter == null) {
                // Singleton - in most case, only one server is used...
                adapter = new RestAdapter(
                        getApplicationContext(), "http://api.darewin.xyz/api");


            }
            return adapter;
        }
}
