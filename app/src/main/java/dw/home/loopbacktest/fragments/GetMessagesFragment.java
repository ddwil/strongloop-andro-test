package dw.home.loopbacktest.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.ImmutableMap;
import com.strongloop.android.loopback.Model;
import com.strongloop.android.loopback.ModelRepository;
import com.strongloop.android.loopback.RestAdapter;
import com.strongloop.android.loopback.callbacks.ListCallback;
import com.strongloop.android.loopback.callbacks.VoidCallback;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import dw.home.loopbacktest.MainActivity;
import dw.home.loopbacktest.R;
import dw.home.loopbacktest.models.Message;
import dw.home.loopbacktest.models.MessageRepository;

/**
 * This fragment will create a new "Message" on each Resume (yes, it's quite stupid, but it's just a test...) and save it online through Strongloop.
 * Then it gets all Messages from online repository and show them.
 */
public class GetMessagesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public GetMessagesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GetMessagesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GetMessagesFragment newInstance(String param1, String param2) {
        GetMessagesFragment fragment = new GetMessagesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_get_messages, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        RestAdapter adapter = ((MainActivity)getActivity()).getLoopBackAdapter();

        MessageRepository repository = adapter.createRepository(MessageRepository.class);
        Log.i("DW%",repository.getNameForRestUrl());
        final Message message= repository.createObject(ImmutableMap.of("name", "Nouveau message"));


        String date = new SimpleDateFormat("dd-MM-yyyy H:m").format(new Date());


        message.setDescription("Ce message a été créé le "+date);
        message.save(new VoidCallback() {
            @Override
            public void onSuccess() {
                // Pencil now exists on the server!
                Toast.makeText(getActivity(), (String) "Success",
                        Toast.LENGTH_LONG).show();
                Log.i("DW%", "Message saved");

            }

            @Override
            public void onError(Throwable t) {
                Toast.makeText(getActivity(), (String) "Error",
                        Toast.LENGTH_LONG).show();
                Log.e("DW%", t.getMessage());
            }
        });


        repository.findAll(new ListCallback<Message>() {

            @Override
            public void onSuccess(List<Message> messages) {
                TextView txtDebug = (TextView)getView().findViewById(R.id.getMessagesFragment_txt_debug);
                String msg = txtDebug.getText().toString();
                Log.i("DW%", String.valueOf(messages.size()));
                for (Message message:messages){
                    Log.i("DW%", message.getDescription());
                    String s=String.format("%s: %s %n", message.getName(), message.getDescription());
                    msg = msg + s; //bad, so bad...
                }
                txtDebug.setText(msg);
                Log.i("DW%", msg);
            }

            @Override
            public void onError(Throwable arg0) {
                Log.e("DW%", "GetAllRewards error: " + arg0);
            }
        });

    }
}
